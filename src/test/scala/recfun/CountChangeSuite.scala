package recfun

import org.scalatest.FunSuite

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class CountChangeSuite extends FunSuite {
  import Main.countChange
  test("countChange: example given in instructions") {
    assert(countChange(4,List(1,2)) === 3)
  }

  test("countChange: sorted CHF") {
    assert(countChange(300,List(5,10,20,50,100,200,500)) === 1022)
  }

  test("countChange: no pennies") {
    assert(countChange(301,List(5,10,20,50,100,200,500)) === 0)
  }

  test("countChange: unsorted CHF") {
    assert(countChange(300,List(500,5,50,100,20,200,10)) === 1022)
  }

  test("countChange: zero CHF results in 0 ways") {
    assert(countChange(0, List(5, 10, 20, 50, 100, 200, 500)) === 0)
  }

  test("countChange: denomination value can be generated in >0 ways") {
    assert(countChange(5, List(5, 10, 20, 50, 100, 200, 500)) > 0)
  }

  test("countChange: empty denomination list results in 0 ways") {
    assert(countChange(172, List()) === 0)
  }

  test("countChange: amounts indivisible by greatest common divisor") {
    assert(countChange(71, List(5, 10, 15, 20, 25, 30, 35, 40, 45, 50)) === 0)
  }

  test("countChange: negative denominations cause a throw") {
    intercept[IllegalArgumentException] {
      countChange(2, List(-1, 2))
    }
  }

  test("countChange: zero denomination causes a throw") {
    intercept[IllegalArgumentException] {
      countChange(2, List(0, 2))
    }
  }
}
