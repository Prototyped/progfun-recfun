package recfun

import org.scalatest.FunSuite

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class PascalSuite extends FunSuite {
  import Main.pascal
  test("pascal: col=0,row=2") {
    assert(pascal(0,2) === 1)
  }

  test("pascal: col=1,row=2") {
    assert(pascal(1,2) === 2)
  }

  test("pascal: col=1,row=3") {
    assert(pascal(1,3) === 3)
  }

  test("pascal: throws IllegalArgumentException if negative row requested") {
    intercept[IllegalArgumentException] {
      pascal(1, -1)
    }
  }

  test("pascal: throws IllegalArgumentException if negative col requested") {
    intercept[IllegalArgumentException] {
      pascal(-1, 1)
    }
  }

  test("pascal: throws IllegalArgumentException if negative row and col req") {
    intercept[IllegalArgumentException] {
      pascal(-2, -3)
    }
  }

  test("pascal: throws IllegalArgumentException if col > row") {
    intercept[IllegalArgumentException] {
      pascal(3, 2)
    }
  }

  test("pascal: top of triangle is 1") {
    assert(pascal(0, 0) === 1)
  }

  test("pascal: left edge of triangle is 1") {
    assert((for (i <- 0 to 10) yield pascal(0, i)).filter(_ != 1).isEmpty)
  }

  test("pascal: right edge of triangle is 1") {
    assert((for (i <- 0 to 10) yield pascal(i, i)).filter(_ != 1).isEmpty)
  }
}
