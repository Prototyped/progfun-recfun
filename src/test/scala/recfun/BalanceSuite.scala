package recfun

import org.scalatest.FunSuite

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class BalanceSuite extends FunSuite {
  import Main.balance

  test("balance: '(if (zero? x) max (/ 1 x))' is balanced") {
    assert(balance("(if (zero? x) max (/ 1 x))".toList))
  }

  test("balance: 'I told him ...' is balanced") {
    assert(balance("I told him (that it's not (yet) done).\n(But he wasn't listening)".toList))
  }

  test("balance: ':-)' is unbalanced") {
    assert(!balance(":-)".toList))
  }

  test("balance: counting is not enough") {
    assert(!balance("())(".toList))
  }

  test("balance: empty list input is balanced") {
    assert(balance(List()))
  }

  test("balance: non-parenthesis input is balanced") {
    assert(balance("some non-parenthesis characters".toList))
  }

  test("balance: open parenthesis input is imbalanced") {
    assert(!balance("some ( text".toList))
  }

  test("balance: close parenthesis input is imbalanced") {
    assert(!balance("some ) text".toList))
  }

  test("balance: a pair of parantheses are balanced") {
    assert(balance("some (text ) ".toList))
  }

  test("balance: more open parentheses than closed are imbalanced") {
    assert(!balance("(one((two)three)".toList))
  }

  test("balance; more close parentheses than open are imbalanced") {
    assert(!balance("(one(two))three)".toList))
  }

  test("balance: more close parentheses than open for a prefix are imbalanced") {
    assert(!balance("(one)())(two".toList))
  }
}
