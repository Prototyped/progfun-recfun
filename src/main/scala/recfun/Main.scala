package recfun
import common._

object Main {
  def main(args: Array[String]) {
    println("Pascal's Triangle")
    for (row <- 0 to 10) {
      for (col <- 0 to row)
        print(pascal(col, row) + " ")
      println()
    }
  }

  /**
   * Exercise 1
   */
  def pascal(c: Int, r: Int): Int = {
    if (c < 0 || r < 0)
      throw new IllegalArgumentException("range for c and r is [0..inf]")
    else if (c > r)
      throw new IllegalArgumentException("c must be less than or equal to r")
    else c match {
      case 0 => 1
      case `r` => 1
      case _ => pascal(c - 1, r - 1) + pascal(c, r - 1)
    }
  }

  /**
   * Exercise 2
   */
  def balance(chars: List[Char]): Boolean = {
    def isBalanced(rest: List[Char], opens: Int, closes: Int): Boolean = {
      if (opens < closes) false
      else rest match {
        case Nil => opens == closes
        case cur :: remaining => cur match {
          case '(' => isBalanced(remaining, opens + 1, closes)
          case ')' => isBalanced(remaining, opens, closes + 1)
          case _ => isBalanced(remaining, opens, closes)
        }
      }
    }

    isBalanced(chars, 0, 0)
  }

  /**
   * Exercise 3
   */
  def countChange(money: Int, coins: List[Int]): Int = {
    def accumulateChange(toAdd: Int, coins: List[Int]): Int = {
      if (toAdd == 0) 1
      else if (toAdd < 0) 0
      else coins match {
        case Nil => 0
        case coin :: rest =>
          if (coin <= 0)
            throw new IllegalArgumentException(
              "coin %d value must be positive".format(coin))
          else
            accumulateChange(toAdd - coin, coins) +
              accumulateChange(toAdd, rest)
      }
    }

    if (money == 0) 0 else accumulateChange(money, coins)
  }
}
